module.exports = {
  "name": "Results by SA1 & CCD (By State) - Percentages",
  "type": "group",
  "items": [
    require('./NSW'),
    require('./ACT'),
    require('./NT'),
    require('./QLD'),
    require('./VIC'),
    require('./TAS'),
    require('./WA')
  ]
};