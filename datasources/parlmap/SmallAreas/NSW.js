module.exports = {
  "name": "NSW",
  "type": "group",
  "items": [
    {
      "name": "2016",
      "opacity": 0.9,
      "type": "csv",
      "url": "parlmap-data/ElectionResults/SmallArea_2016/2016_SA1_NSW.csv",
      "tableStyle": {
        "dataVariable": "TCP_elected_candidate_party",
        "columns": {
          "1st_preference_votes_ALP_pc": require('../styling/FirstPreference-ALP'),
          "1st_preference_votes_LP_pc": require('../styling/FirstPreference-Liberal'),
          "1st_preference_votes_LNP_pc": require('../styling/FirstPreference-Liberal'),
          "1st_preference_votes_CLP_pc": {},
          "1st_preference_votes_NP_pc": require('../styling/FirstPreference-National'),
          "1st_preference_votes_GRN_pc": require('../styling/FirstPreference-Greens'),
          "1st_preference_votes_XEN_pc": require('../styling/NoStyling'),
          "1st_preference_votes_IND1_pc": require('../styling/FirstPreference-Independent'),
          "1st_preference_votes_Others_pc": require('../styling/FirstPreference-Other'),
          "TCP_elected_candidate_name": require('../styling/HiddenColumn'),
          "TCP_elected_candidate_party": require('../styling/PartyEnum'),
          "TCP_elected_candidate_votes_no": require('../styling/HiddenColumn'),
          "TCP_elected_candidate_votes_pc": {},
          "TCP_elected_candidate_margin_pc": {},
          "TCP_other_candidate_name": require('../styling/HiddenColumn'),
          "TCP_other_candidate_party": require('../styling/PartyEnum'),
          "TCP_other_candidate_votes_no": require('../styling/HiddenColumn'),
          "TCP_other_candidate_votes_pc": {},
          "TCP_other_candidate_margin_pc": {},
          "TPP_ALP_votes_pc": require('../styling/TPP-ALP'),
          "TPP_ALP_margin_pc": {},
          "TPP_LPNP_votes_pc": require('../styling/TPP-LNP'),
          "TTP_LPNP_margin_pc": {}
        }
      }
    }
  ]
};