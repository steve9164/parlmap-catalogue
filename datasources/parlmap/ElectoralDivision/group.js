module.exports = {
  "items": [
    {
      "name": "2007",
      "opacity": 0.9,
      "type": "csv",
      "url": "parlmap-data/ElectionResults/ElectoralDivision_2007-2016/2007_CED.csv",
      "tableStyle": {
        "dataVariable": "TCP_elected_candidate_party",
        "columns": {
          "Enrolled_no": {},
          "Formal_votes_pc": require('../styling/Formal'),
          "Informal_votes_pc": require('../styling/Informal'),
          "1st_preference_votes_ALP_pc": require('../styling/FirstPreference-ALP'),
          "1st_preference_votes_LP_pc": require('../styling/FirstPreference-Liberal'),
          "1st_preference_votes_NP_pc": require('../styling/FirstPreference-National'),
          "1st_preference_votes_GRN_pc": require('../styling/FirstPreference-Greens'),
          "1st_preference_votes_IND1_pc": require('../styling/FirstPreference-Independent'),
          "1st_preference_votes_Others_pc": require('../styling/FirstPreference-Other'),
          "TCP_elected_candidate_name": require('../styling/HiddenColumn'),
          "TCP_elected_candidate_votes_no": require('../styling/HiddenColumn'),
          "TCP_elected_candidate_votes_pc": require('../styling/HiddenColumn'),
          "TCP_elected_candidate_party": require('../styling/PartyEnum'),
          "TCP_other_candidate_name": require('../styling/HiddenColumn'),
          "TCP_other_candidate_votes_no": require('../styling/HiddenColumn'),
          "TCP_other_candidate_votes_pc": require('../styling/HiddenColumn'),
          "TCP_other_candidate_party": require('../styling/PartyEnum'),
          "TPP_ALP_votes_pc": require('../styling/TPP-ALP'),
          "TPP_LPNP_votes_pc": require('../styling/TPP-LNP')
        }
      }
    },
    {
      "name": "2010",
      "opacity": 0.9,
      "type": "csv",
      "url": "parlmap-data/ElectionResults/ElectoralDivision_2007-2016/2010_CED.csv",
      "tableStyle": {
        "dataVariable": "TCP_elected_candidate_party",
        "columns": {
          "Enrolled_no": {},
          "Formal_votes_pc": require('../styling/Formal'),
          "Informal_votes_pc": require('../styling/Informal'),
          "1st_preference_votes_ALP_pc": require('../styling/FirstPreference-ALP'),
          "1st_preference_votes_LP_pc": require('../styling/FirstPreference-Liberal'),
          "1st_preference_votes_LNP_pc": require('../styling/FirstPreference-Liberal'),
          "1st_preference_votes_NP_pc": require('../styling/FirstPreference-National'),
          "1st_preference_votes_GRN_pc": require('../styling/FirstPreference-Greens'),
          "1st_preference_votes_IND1_pc": require('../styling/FirstPreference-Independent'),
          "1st_preference_votes_Others_pc": require('../styling/FirstPreference-Other'),
          "TCP_elected_candidate_name": require('../styling/HiddenColumn'),
          "TCP_elected_candidate_votes_no": require('../styling/HiddenColumn'),
          "TCP_elected_candidate_votes_pc": require('../styling/HiddenColumn'),
          "TCP_elected_candidate_party": require('../styling/PartyEnum'),
          "TCP_other_candidate_name": require('../styling/HiddenColumn'),
          "TCP_other_candidate_votes_no": require('../styling/HiddenColumn'),
          "TCP_other_candidate_votes_pc": require('../styling/HiddenColumn'),
          "TCP_other_candidate_party": require('../styling/PartyEnum'),
          "TPP_ALP_votes_pc": require('../styling/TPP-ALP'),
          "TPP_LPNP_votes_pc": require('../styling/TPP-LNP')
        }
      }
    },
    {
      "name": "2013",
      "opacity": 0.9,
      "type": "csv",
      "url": "parlmap-data/ElectionResults/ElectoralDivision_2007-2016/2013_CED.csv",
      "tableStyle": {
        "dataVariable": "TCP_elected_candidate_party",
        "columns": {
          "Enrolled_no": {},
          "Formal_votes_pc": require('../styling/Formal'),
          "Informal_votes_pc": require('../styling/Informal'),
          "1st_preference_votes_ALP_pc": require('../styling/FirstPreference-ALP'),
          "1st_preference_votes_LP_pc": require('../styling/FirstPreference-Liberal'),
          "1st_preference_votes_LNP_pc": require('../styling/FirstPreference-Liberal'),
          "1st_preference_votes_NP_pc": require('../styling/FirstPreference-National'),
          "1st_preference_votes_GRN_pc": require('../styling/FirstPreference-Greens'),
          "1st_preference_votes_PUP_pc": require('../styling/FirstPreference-PUP'),
          "1st_preference_votes_KAP_pc": require('../styling/FirstPreference-KAP'),
          "1st_preference_votes_IND1_pc": require('../styling/FirstPreference-Independent'),
          "1st_preference_votes_Others_pc": require('../styling/FirstPreference-Other'),
          "TCP_elected_candidate_name": require('../styling/HiddenColumn'),
          "TCP_elected_candidate_votes_no": require('../styling/HiddenColumn'),
          "TCP_elected_candidate_votes_pc": require('../styling/HiddenColumn'),
          "TCP_elected_candidate_party": require('../styling/PartyEnum'),
          "TCP_other_candidate_name": require('../styling/HiddenColumn'),
          "TCP_other_candidate_votes_no": require('../styling/HiddenColumn'),
          "TCP_other_candidate_votes_pc": require('../styling/HiddenColumn'),
          "TCP_other_candidate_party": require('../styling/PartyEnum'),
          "TPP_ALP_votes_pc": require('../styling/TPP-ALP'),
          "TPP_LPNP_votes_pc": require('../styling/TPP-LNP')
        }
      }
    },
    {
      "name": "2016",
      "opacity": 0.9,
      "type": "csv",
      "url": "parlmap-data/ElectionResults/ElectoralDivision_2007-2016/2016_CED.csv",
      "tableStyle": {
        "dataVariable": "TCP_elected_candidate_party",
        "columns": {
          "Enrolled_no": {},
          "Formal_votes_pc": require('../styling/Formal'),
          "Informal_votes_pc": require('../styling/Informal'),
          "1st_preference_votes_ALP_pc": require('../styling/FirstPreference-ALP'),
          "1st_preference_votes_LP_pc": require('../styling/FirstPreference-Liberal'),
          "1st_preference_votes_LNP_pc": require('../styling/FirstPreference-Liberal'),
          "1st_preference_votes_NP_pc": require('../styling/FirstPreference-National'),
          "1st_preference_votes_GRN_pc": require('../styling/FirstPreference-Greens'),
          "1st_preference_votes_KAP_pc": require('../styling/FirstPreference-KAP'),
          "1st_preference_votes_XEN_pc": {},
          "1st_preference_votes_IND1_pc": require('../styling/FirstPreference-Independent'),
          "1st_preference_votes_Others_pc": require('../styling/FirstPreference-Other'),
          "TCP_elected_candidate_name": require('../styling/HiddenColumn'),
          "TCP_elected_candidate_votes_no": require('../styling/HiddenColumn'),
          "TCP_elected_candidate_votes_pc": require('../styling/HiddenColumn'),
          "TCP_elected_candidate_party": require('../styling/PartyEnum'),
          "TCP_other_candidate_name": require('../styling/HiddenColumn'),
          "TCP_other_candidate_votes_no": require('../styling/HiddenColumn'),
          "TCP_other_candidate_votes_pc": require('../styling/HiddenColumn'),
          "TCP_other_candidate_party": require('../styling/PartyEnum'),
          "TPP_ALP_votes_pc": require('../styling/TPP-ALP'),
          "TPP_LPNP_votes_pc": require('../styling/TPP-LNP')
        }
      }
    }
  ],
  "name": "Results by Commonwealth Electoral Division",
  "type": "group"
}