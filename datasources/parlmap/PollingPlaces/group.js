module.exports = {
  "name": "Results by Polling Booth",
  "type": "group",
  "preserveOrder": true,
  "items": [
    require('./NSW'),
    require('./VIC'),
    require('./QLD'),
    require('./WA'),
    require('./SA'),
    require('./TAS'),
    require('./ACT'),
    require('./NT')
  ]
};