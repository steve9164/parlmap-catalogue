module.exports = {
  "catalog": [
    {
      "items": [
        require('./SmallAreas/group'),
        require('./ElectoralDivision/group'),
        require('./PollingPlaces/group')
      ],
      "name": "Federal Election Results",
      "type": "group"
    }
  ],
  "services": []
};