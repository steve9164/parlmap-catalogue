const fs = require('fs');
const root = require('./parlmap/root');

module.exports = function buildCatalog() {
    fs.writeFileSync('parlmap.json', JSON.stringify(root));
};
